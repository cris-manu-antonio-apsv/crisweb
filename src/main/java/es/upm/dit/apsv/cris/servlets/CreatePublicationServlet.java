package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;


import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;


@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	final String ADMIN = "root";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	   
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	   
		String id = (String) request.getParameter("id");
	       String title = request.getParameter("title");
	       String publicationName = request.getParameter("publicationName");
	       String publicationDate = request.getParameter("publicationDate");
	       String authors = request.getParameter("authors");
	       int citeCount = Integer.parseInt(request.getParameter("citeCount"));

	       Publication Publication = new Publication();
	       Publication.setId(id);
	       Publication.setTitle(title);
	       Publication.setPublicationName(publicationName);
	       Publication.setPublicationDate(publicationDate);
	       Publication.setAuthors(authors);
	       Publication.setCiteCount(citeCount);

	       
	       
	       ClientBuilder.newClient(new ClientConfig())
           .target(URLHelper.getInstance().getCrisURL() + "/rest/Publications").request()
           .post(Entity.entity(Publication, MediaType.APPLICATION_JSON), Publication.class);
	       response.sendRedirect(request.getContextPath() + "/AdminServlet");

		
	}



}
