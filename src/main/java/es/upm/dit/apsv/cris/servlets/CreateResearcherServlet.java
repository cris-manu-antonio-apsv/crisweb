package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;


import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Researcher;


@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	final String ADMIN = "root";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	   
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	   
			String id = (String) request.getParameter("id");
	       String name = request.getParameter("name");
	       String lastname = request.getParameter("lastname");
	       String email = request.getParameter("email");

	       Researcher Researcher = new Researcher();
	       Researcher.setId(id);
	       Researcher.setName(name);
	       Researcher.setLastname(lastname);
	       Researcher.setEmail(email);
	       
	       
	       ClientBuilder.newClient(new ClientConfig())
           .target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers").request()
           .post(Entity.entity(Researcher, MediaType.APPLICATION_JSON), Researcher.class);
	       response.sendRedirect(request.getContextPath() + "/AdminServlet");

		
	}



}
