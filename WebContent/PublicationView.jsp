<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Publication</title>
<%@ include file = "Header.jsp"%>
</head>
<body>
<table>

<tr>
<th>Id</th><th>Title</th><th>Publication Name</th><th>Publication Date</th><th>Authors Id</th>
</tr>
        <tr>
  				<td>${pi.id}</td>
                <td>${pi.title}</td>
                <td>${pi.publicationName}</td>
                <td>${pi.publicationDate}</a></td>
                <td>${pi.authors}</td>
        </tr>
</table>

</body>
</html>